import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { DataService } from '../../../services/data.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {

  userById: any
  currentUserId: any
  stats = [
    {
      title: 'Select',
      value: ''
    },
    {
      title: 'Single',
      value: 'Single'
    },
    {
      title: 'Married',
      value: 'Married'
    }
  ]

  constructor(private activatedRoute: ActivatedRoute, public router: Router, private data: DataService, private fb: FormBuilder) { }

  updateForm = this.fb.group({
    name: ['', Validators.required],
    phone: [''],
    dob: [''],
    status: [''],
    address: ['']
  })

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(({ id }) => {
      this.data.getUserById(id).subscribe(res => {
        this.userById = res
        this.currentUserId = res._id
      })
    });
  }

  save() {
    console.log(this.updateForm.value);
    this.data.updateInfo(this.currentUserId, this.updateForm.value).subscribe((res) => {
      if (res) {
        this.router.navigate(['users/user-details'])
      }
    })
    
  }

  back() {
    this.router.navigate(['users/user-details'])
  }

}
