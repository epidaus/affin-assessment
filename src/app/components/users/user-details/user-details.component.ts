import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from '@angular/router';
import { MatDialog  } from '@angular/material/dialog';

import { DataService } from '../../../services/data.service';
import { ConfirmationDialogComponent } from '../../confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {

  userData: []
  user: any
  currentUserId: any
  constructor(public router: Router, private data: DataService, private spinner: NgxSpinnerService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.spinner.show()

    this.data.getProfile().subscribe(x => {
      this.currentUserId = x.user._id
    })
    
    setTimeout(() => {
    this.data.getAllUsers().subscribe(res => {
      this.spinner.hide();
      this.userData = res.filter((user) => 
      user._id !== this.currentUserId
      )
      
    })
  }, 500);
  }

  getComplete(user, value) {
    const findUser: any = this.userData.find((u: any) => u._id === user._id)
    const userTask = findUser.task.filter((task) => task.status === value).length
    return userTask
  }

  refresh() {
    this.ngOnInit()
  }

  deleteUser(id) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Do you confirm the deletion of this User?"
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.data.deleteUserById(id).subscribe(res => {
          if(res) {
            this.refresh()
          }
        })
      }
    });

    
  }
  

}
