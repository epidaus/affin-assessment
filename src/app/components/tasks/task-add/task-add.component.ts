import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";

import { DataService } from '../../../services/data.service';

@Component({
  selector: 'app-task-add',
  templateUrl: './task-add.component.html',
  styleUrls: ['./task-add.component.scss']
})
export class TaskAddComponent implements OnInit {

  stats = [
    {
      title: 'Select',
      value: ''
    },
    {
      title: 'Completed',
      value: 'completed'
    },
    {
      title: 'Not-Completed',
      value: 'not-completed'
    }
  ]

  userData: any
  userName: any

  constructor(private data: DataService, private fb: FormBuilder, public router: Router, private spinner: NgxSpinnerService) { }

  taskForm = this.fb.group({
    title: ['', Validators.required],
    desc: ['', Validators.required],
    status: ['', Validators.required],
    people: ['', Validators.required]
  })

  ngOnInit(): void {
    this.data.getAllUsers().subscribe(res => {
      this.userData = res
      
    })
  }

  allUser() {
    
  }

  add() {
    console.log(this.taskForm.value);
    this.data.addTask(this.taskForm.value.people, this.taskForm.value).subscribe((res) => {
      if (res) {
        this.router.navigate(['tasks/task-list'])
      }
    })
    
  }

  back() {
    this.router.navigate(['tasks/task-list'])
  }

}
