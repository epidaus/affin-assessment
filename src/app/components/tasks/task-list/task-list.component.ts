import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";

import { DataService } from '../../../services/data.service'

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {

  allTask: any
  constructor(private data: DataService, private spinner: NgxSpinnerService) { }

  ngOnInit(): void {

    this.spinner.show()
        setTimeout(() => {
          this.data.listTask().subscribe(res => {
            this.spinner.hide()
            this.allTask = res
            
          })
        }, 500);

  }

}
