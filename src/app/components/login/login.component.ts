import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import {NgbAlertConfig} from '@ng-bootstrap/ng-bootstrap';


import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @Input() public alerts: Array<string> = [];
  error: any
  errorMsg: any
  // successMsg: any
  alertShow = false
  // successShow = false

  constructor(
    private data: DataService,
    private fb: FormBuilder,
    public router: Router,
    private spinner: NgxSpinnerService,
    alertConfig: NgbAlertConfig
    ) {
      alertConfig.type = 'danger';
      alertConfig.dismissible = false;
    }

  ngOnInit(): void {
    this.spinner.show()
        setTimeout(() => {
          this.spinner.hide();
        }, 500);
  }

  loginForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  })

  log_in() {
    this.data.login(this.loginForm.value).subscribe((res) => {
      if (res) {
        // this.successShow = true
        // this.successMsg = res.msg
        localStorage.setItem('token', res.token)
        this.router.navigate(['home'])
      }
    }, (error) => {
      // console.log(error.error.msg);
      this.errorMsg = error.error.msg
      this.alertShow = true
    })
  }

}
