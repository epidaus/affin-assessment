import { Component, OnInit } from '@angular/core';

import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  profileData: any

  constructor(private data: DataService) { }

  ngOnInit(): void {

    this.data.getProfile().subscribe(res => {
      this.profileData = res.user
    })
  }

}
