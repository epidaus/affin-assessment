import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";

import { DataService } from '../../../services/data.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  profileData: any
  myTaskComplete: any
  myTaskNotComplete: any
  countComplete: any
  countNotComplete: any

  constructor(private data: DataService, private spinner: NgxSpinnerService) { }

  ngOnInit(): void {

    this.spinner.show()
        setTimeout(() => {

          this.data.getProfile().subscribe(res => {
            this.spinner.hide()
            this.profileData = res.user

            this.data.listTaskByUser(this.profileData._id).subscribe(task => {
              this.myTaskComplete = task.filter((complete) => complete.status === 'completed' )
              this.myTaskNotComplete = task.filter((xcomplete) => xcomplete.status === 'not-completed' )
              // console.log(this.myTask.length);
              this.countComplete = this.myTaskComplete.length
              this.countNotComplete = this.myTaskNotComplete.length
              
            })
            
          })

          

        }, 500);
  }

}
