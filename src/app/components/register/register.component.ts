import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";

import { ValidationService } from '../../services/validation.service';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  errorMsg: any
  alertShow = false

  constructor(private data: DataService, private fb: FormBuilder, public router: Router, private spinner: NgxSpinnerService) { }

  registerForm = this.fb.group({
    name: ['', Validators.required],
    username: ['', [Validators.required, Validators.minLength(5)]],
    email: ['', [Validators.required, ValidationService.emailValidator]],
    password: ['', [Validators.required, ValidationService.passwordValidator]],
    confirm_password: ['', Validators.required]
  },
  { 
    validators: this.password.bind(this)
  })

  ngOnInit(): void {
    this.spinner.show()
    setTimeout(() => {
      this.spinner.hide();
    }, 500)
  }

  password(formGroup: FormGroup) {
    const { value: password } = formGroup.get('password');
    const { value: confirmPassword } = formGroup.get('confirm_password');
    return password === confirmPassword ? null : { passwordNotMatch: true };
  }

  reg() {
    if (this.registerForm.dirty && this.registerForm.valid) {
      this.data.register(this.registerForm.value).subscribe((res) => {
        if (res) {
          this.router.navigate(['login'])
          
        }
      }, (error) => {
        this.errorMsg = error.error.msg
        this.alertShow = true
      })
    }
    
  }

}
